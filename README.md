Justyns dotfiles
=================

Description
-----------
This is a collection of various configs(dotfiles) I like to use in my shells

Installation
-----------
    cd
    git clone git@github.com:justyns/jdotfiles.git .jdotfiles
    cd .jdotfiles
    git submodule init
    git submodule update
    ./install.sh
If any files or directories exist already, move them out of the way and re-run install.sh

Updates
-------
    cd ~/.jdotfiles
    git pull
    git submodule update
    ./install.sh
